/* eslint-env mocha */
'use strict'

const { describe } = require('mocha')
const { deferExceptions, expect, removeSyncForce } = require('./test-utils')
const ContentCatalog = require('@antora/content-classifier/lib/content-catalog')

const fs = require('fs')
const fsp = fs.promises
const getCacheDir = require('cache-directory')
const http = require('http')
const { register } = require('../lib/import-javadoc')
const os = require('os')
const ospath = require('path')

const JAVADOC_CACHE_FOLDER = '@djencks/antora-javadoc'
const CACHE_DIR = getCacheDir('antora-test')
const JAVADOC_CACHE_DIR = ospath.join(CACHE_DIR, 'content', '2', JAVADOC_CACHE_FOLDER)
const CWD = process.cwd()
const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const WORK_DIR = ospath.join(__dirname, 'work')
const BUNDLE_TYPE = 'JavaDoc jar'

const TEST_JAR = 'geronimo-transaction-3.1.4-javadoc.jar'

//when https://gitlab.com/antora/antora/-/issues/525 is implemented use of contentAggregate may be removed.
async function importJavadoc (playbook, contentAggregate, contentCatalog, config) {
  const pipeline = {}
  const on = (name, funct) => (pipeline[name] = funct)
  const require = (req) => (name) => {
    return {
      debug: (msg) => { console.log(msg) },
      warn: (msg) => { console.warn(msg) },
    }
  }
  register({ on, require }, { playbook, config })
  if (contentAggregate) pipeline.contentAggregated({ playbook, contentAggregate })
  await pipeline.contentClassified({ playbook, contentCatalog })
  return contentCatalog
}

describe('importJavadoc()', () => {
  const expectedFilePaths = [
    // 'META-INF/MANIFEST.MF',
    'allclasses-frame.html',
    'allclasses-noframe.html',
    'constant-values.html',
    'deprecated-list.html',
    'help-doc.html',
    'index-all.html',
    'index.html',
    'org/apache/geronimo/transaction/class-use/GeronimoUserTransaction.html',
    'org/apache/geronimo/transaction/GeronimoUserTransaction.html',
    'org/apache/geronimo/transaction/log/class-use/HOWLLog.html',
    'org/apache/geronimo/transaction/log/class-use/UnrecoverableLog.html',
    'org/apache/geronimo/transaction/log/class-use/XidImpl2.html',
    'org/apache/geronimo/transaction/log/HOWLLog.html',
    'org/apache/geronimo/transaction/log/package-frame.html',
    'org/apache/geronimo/transaction/log/package-summary.html',
    'org/apache/geronimo/transaction/log/package-tree.html',
    'org/apache/geronimo/transaction/log/package-use.html',
    'org/apache/geronimo/transaction/log/UnrecoverableLog.html',
  ]

  let server
  let serverRequests
  let playbook

  const prefixPath = (prefix, path_) => [prefix, path_].join(ospath.sep)

  const testAll = (archive, testBlock, playbook = {}, suppliedConfig) => {
    const makeTest =
      async (archive, configInPlaybook = false) => {
        if (suppliedConfig) {
          suppliedConfig.sources[0].url = archive
        }
        await testBlock(...setupInputs(playbook, suppliedConfig || config(archive), {}, configInPlaybook))
      }
    it('with dot-relative bundle path', async () =>
      await makeTest(prefixPath('.', ospath.relative(WORK_DIR, ospath.join(FIXTURES_DIR, archive)))))
    it('with absolute bundle path', async () =>
      await makeTest(ospath.join(FIXTURES_DIR, archive)))
    it('with remote bundle URI', async () =>
      await makeTest('http://localhost:1337/' + archive))
    it('with remote bundle URI, config in playbook', async () =>
      await makeTest('http://localhost:1337/' + archive, true))
  }

  function config (url, snapshot, layout, extract, atSource = true) {
    const source = { url }
    const config = { sources: [source] }
    if (snapshot !== undefined) atSource ? source.snapshot = snapshot : config.snapshot = snapshot
    layout && (atSource ? source.layout = layout : config.layout = layout)
    extract && (atSource ? source.extract = extract : config.extract = extract)
    return config
  }

  function setupInputs (playbook, config, playbookConfig = {}, configInPlaybook = false, version = '1.0') {
    const contentCatalog = new ContentCatalog()
    playbook = playbook || {}
    playbookConfig.require = JAVADOC_CACHE_FOLDER
    const descriptor = { name: 'foo', version }
    if (configInPlaybook) {
      playbookConfig.components = [Object.assign({ name: 'foo', version }, config)]
    } else {
      config.require = JAVADOC_CACHE_FOLDER
      descriptor.pipeline = { extensions: [config] }
      // playbookConfig.config = {}
    }
    playbook.pipeline = { extensions: [playbookConfig] }
    const contentAggregate = [descriptor]
    contentCatalog.registerComponentVersion(descriptor.name, descriptor.version, descriptor)
    return [playbook, contentAggregate, contentCatalog, playbookConfig, config]
  }

  const clean = (fin) => {
    process.chdir(CWD)
    removeSyncForce(CACHE_DIR)
    removeSyncForce(WORK_DIR)
    if (!fin) {
      fs.mkdirSync(WORK_DIR, { recursive: true })
      process.chdir(WORK_DIR)
    }
  }

  beforeEach(() => {
    clean()
    playbook = {}
    serverRequests = []
    server = http
      .createServer((request, response) => {
        serverRequests.push(request.url)
        fs.readFile(ospath.join(__dirname, 'fixtures', request.url), (err, content) => {
          if (err) {
            response.writeHead(404, { 'Content-Type': 'text/html' })
            response.end('<!DOCTYPE html><html><body>Not Found</body></html>', 'utf8')
          } else {
            response.writeHead(200, { 'Content-Type': 'application/zip' })
            response.end(content)
          }
        })
      })
      .listen(1337)
  })

  afterEach(() => {
    clean(true)
    server.close()
  })

  describe('should throw error if bundle cannot be found', () => {
    testAll('no-such-bundle.jar', async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
      const importJavadocDeferred =
        await deferExceptions(importJavadoc, playbook, contentAggregate, contentCatalog, playbookConfig)
      const javadoc = contentAggregate[0].pipeline
        ? contentAggregate[0].pipeline.extensions[0].sources[0]
        : playbook.pipeline.extensions[0].components[0].sources[0]
      if (javadoc.url.startsWith('http://')) {
        const expectedMessage = `Unexpected statusCode 404 for ${javadoc.url}`
        expect(importJavadocDeferred)
          .to.throw(expectedMessage)
      } else {
        const expectedMessage = /Could not read local file file:\/\/[^ ]*/
        expect(importJavadocDeferred).to.throw(expectedMessage)
      }
    }, playbook)
  })

  describe('should throw error if bundle is not a valid jar file', () => {
    testAll('the-javadoc-jar.tar.gz', async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
      const javadoc = contentAggregate[0].pipeline
        ? contentAggregate[0].pipeline.extensions[0].sources[0]
        : playbook.pipeline.extensions[0].components[0].sources[0]
      const isRemote = javadoc.url.startsWith('http://')
      const isAbsolute = ospath.isAbsolute(javadoc.url)
      const url = isRemote ? javadoc.url : isAbsolute ? `file://${javadoc.url}` : `file://${ospath.join(FIXTURES_DIR, javadoc.url)}`
      const expectedMessage = `Invalid ${BUNDLE_TYPE}: '${url}'`
      expect(await deferExceptions(importJavadoc, playbook, contentAggregate, contentCatalog, playbookConfig))
        .to.throw(expectedMessage)
        .with.property('stack')
        .that.includes('not a valid zip file')
    }, playbook)
  })

  describe(`should load all files in the ${BUNDLE_TYPE}`, () => {
    testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
      await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
      const relativePaths = files.map((file) => file.relative)
      expect(paths).to.eql(relativePaths)
    }, playbook)
  })

  ;[{ layout: 'foo' },
    { defaultLayout: 'foo' },
    { playbookLayout: 'foo' },
    { layout: 'foo', playbookLayout: 'bar' },
    { defaultLayout: 'foo', playbookLayout: 'bar' },
    { layout: 'foo', defaultLayout: 'bar', playbookLayout: 'baz' },
  ].forEach((layouts) => {
    describe(`should set layout from ${JSON.stringify(layouts)}`, () => {
      const config = {
        require: JAVADOC_CACHE_FOLDER,
        sources: [{}],
      }
      layouts.layout && (config.sources[0].layout = layouts.layout)
      layouts.defaultLayout && (config.layout = layouts.defaultLayout)
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        if (layouts.playbookLayout) {
          playbookConfig.layout = layouts.playbookLayout
        }
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const files = contentCatalog.getAll()
        const paths = files.map((file) => file.path)
        expect(paths).to.include.members(expectedFilePaths)
        const relativePaths = files.map((file) => file.relative)
        expect(paths).to.eql(relativePaths)
        files.forEach((file) => expect(file.asciidoc.attributes['page-layout']).to.equal(file.src.extname === '.html' ? 'foo' : 'bare'))
      }, playbook, config)
    })
  }
  )

  describe('should expand local jar path', () => {
    it('should append unanchored bundle path to cwd', async () => {
      const playbookDir = ospath.join(WORK_DIR, 'some-other-folder')
      const playbook = { dir: playbookDir }
      fs.mkdirSync(playbookDir, { recursive: true })
      const bundleFixture = ospath.join(FIXTURES_DIR, TEST_JAR)
      fs.writeFileSync(TEST_JAR, fs.readFileSync(bundleFixture))
      // playbook.ui = { bundle: { url: TEST_JAR } }
      let contentCatalog
      const importJavadocDeferred = await deferExceptions(importJavadoc, ...setupInputs(playbook, config(TEST_JAR)))
      expect(() => (contentCatalog = importJavadocDeferred())).to.not.throw()
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    })

    it('should expand leading . segment in bundle path to playbook dir', async () => {
      const playbook = { dir: WORK_DIR }
      const newWorkDir = ospath.join(WORK_DIR, 'some-other-folder')
      fs.mkdirSync(newWorkDir, { recursive: true })
      process.chdir(newWorkDir)
      const url = prefixPath('.', ospath.relative(WORK_DIR, ospath.join(FIXTURES_DIR, TEST_JAR)))
      let contentCatalog
      const importJavadocDeferred = await deferExceptions(importJavadoc, ...setupInputs(playbook, config(url)))
      expect(() => (contentCatalog = importJavadocDeferred())).to.not.throw()
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    })

    it('should expand leading ~ segment in bundle path to user home', async () => {
      const playbook = {}
      const url = prefixPath('~', ospath.relative(os.homedir(), ospath.join(FIXTURES_DIR, TEST_JAR)))
      let contentCatalog
      const importJavadocDeferred = await deferExceptions(importJavadoc, ...setupInputs(playbook, config(url)))
      expect(() => (contentCatalog = importJavadocDeferred())).to.not.throw()
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    })

    it('should expand leading ~+ segment in bundle path to cwd', async () => {
      const playbook = { dir: WORK_DIR }
      const newWorkDir = ospath.join(WORK_DIR, 'some-other-folder')
      fs.mkdirSync(newWorkDir, { recursive: true })
      process.chdir(newWorkDir)
      const url = prefixPath('~+', ospath.relative(newWorkDir, ospath.join(FIXTURES_DIR, TEST_JAR)))
      let contentCatalog
      const importJavadocDeferred = await deferExceptions(importJavadoc, ...setupInputs(playbook, config(url)))
      expect(() => (contentCatalog = importJavadocDeferred())).to.not.throw()
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    })
  })

  describe('should locate bundle when cwd and playbook dir are different', () => {
    testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
      playbook.dir = WORK_DIR
      const newWorkDir = ospath.join(WORK_DIR, 'some-other-folder')
      fs.mkdirSync(newWorkDir, { recursive: true })
      process.chdir(newWorkDir)
      // let contentCatalog
      const importJavadocDeferred =
        await deferExceptions(importJavadoc, playbook, contentAggregate, contentCatalog, playbookConfig)
      expect(() => (contentCatalog = importJavadocDeferred())).to.not.throw()
      const files = contentCatalog.getAll()
      const paths = files.map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    }, {})
  })

  describe('should set the out property on assets', () => {
    testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
      await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
      const pages = contentCatalog.getAll()
      pages.forEach((file) => {
        expect(file).to.have.property('out')
      })
      const indexAll = pages.find(({ path: p }) => p === 'index-all.html')
      expect(indexAll).to.exist()
      expect(indexAll.out).to.eql({
        basename: 'index-all.html',
        dirname: 'foo/1.0/javadoc',
        moduleRootPath: '.',
        path: 'foo/1.0/javadoc/index-all.html',
        rootPath: '../../..',
      })
    }, playbook)
  })

  describe('should use module and topic settings in javadoc key', () => {
    describe('module setting', () => {
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const pages = contentCatalog.getAll()
        pages.forEach((file) => {
          expect(file).to.have.property('out')
        })
        const indexAll = pages.find(({ path: p }) => p === 'index-all.html')
        expect(indexAll).to.exist()
        expect(indexAll.out).to.eql({
          basename: 'index-all.html',
          dirname: 'foo/1.0/module1',
          moduleRootPath: '.',
          path: 'foo/1.0/module1/index-all.html',
          rootPath: '../../..',
        })
      }, playbook, {
        path: JAVADOC_CACHE_FOLDER,
        sources: [{ module: 'module1' }],
      })
    })

    describe('topic setting', () => {
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const pages = contentCatalog.getAll()
        pages.forEach((file) => {
          expect(file).to.have.property('out')
        })
        const indexAll = pages.find(({ path: p }) => p === 'index-all.html')
        expect(indexAll).to.exist()
        expect(indexAll.out).to.eql({
          basename: 'index-all.html',
          dirname: 'foo/1.0/javadoc/topic/subtopic',
          moduleRootPath: '../..',
          path: 'foo/1.0/javadoc/topic/subtopic/index-all.html',
          rootPath: '../../../../..',
        })
      }, playbook, {
        path: JAVADOC_CACHE_FOLDER,
        sources: [{ topic: 'topic/subtopic' }],
      })
    })

    describe('module and topic setting', () => {
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const pages = contentCatalog.getAll()
        pages.forEach((file) => {
          expect(file).to.have.property('out')
        })
        const indexAll = pages.find(({ path: p }) => p === 'index-all.html')
        expect(indexAll).to.exist()
        expect(indexAll.out).to.eql({
          basename: 'index-all.html',
          dirname: 'foo/1.0/module1/topic/subtopic',
          moduleRootPath: '../..',
          path: 'foo/1.0/module1/topic/subtopic/index-all.html',
          rootPath: '../../../../..',
        })
        const script = pages.find(({ path: p }) => p === 'script.js')
        expect(script).to.not.exist()
      }, playbook, {
        path: JAVADOC_CACHE_FOLDER,
        sources: [{ module: 'module1', topic: 'topic/subtopic' }],
      })
    })

    describe('module and topic setting, override include pattern', () => {
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const pages = contentCatalog.getAll()
        pages.forEach((file) => {
          expect(file).to.have.property('out')
        })
        const script = pages.find(({ path: p }) => p === 'script.js')
        expect(script).to.exist()
        expect(script.out).to.eql({
          basename: 'script.js',
          dirname: 'foo/1.0/module1/topic/subtopic',
          moduleRootPath: '../..',
          path: 'foo/1.0/module1/topic/subtopic/script.js',
          rootPath: '../../../../..',
        })
        const stylesheet = pages.find(({ path: p }) => p === 'stylesheet.css')
        expect(stylesheet).to.exist()
      }, playbook, {
        path: JAVADOC_CACHE_FOLDER,
        sources: [{ module: 'module1', topic: 'topic/subtopic' }],
        includes: '*',
      })
    })

    describe('module and topic setting, override includes and excludes patterns', () => {
      testAll(TEST_JAR, async (playbook, contentAggregate, contentCatalog, playbookConfig) => {
        await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
        const pages = contentCatalog.getAll()
        pages.forEach((file) => {
          expect(file).to.have.property('out')
        })
        const script = pages.find(({ path: p }) => p === 'script.js')
        expect(script).to.exist()
        expect(script.out).to.eql({
          basename: 'script.js',
          dirname: 'foo/1.0/module1/topic/subtopic',
          moduleRootPath: '../..',
          path: 'foo/1.0/module1/topic/subtopic/script.js',
          rootPath: '../../../../..',
        })
        const stylesheet = pages.find(({ path: p }) => p === 'stylesheet.css')
        expect(stylesheet).to.not.exist()
      }, playbook, {
        path: JAVADOC_CACHE_FOLDER,
        sources: [{ module: 'module1', topic: 'topic/subtopic' }],
        includes: '*,!stylesheet.css',
      })
    })
  })

  it('should expand version glob pattern in playbook components and substitute into url', async () => {
    const url = `http://localhost:1337/${TEST_JAR}`.replace('3.1.4', '{version}')
    let [playbook, contentAggregate, contentCatalog, playbookConfig, descriptorConfig] = setupInputs({}, config(url), {}, true, '3.1.4')
    playbookConfig.components = [{ name: 'foo', version: '*', sources: descriptorConfig.sources }]
    contentCatalog = await importJavadoc(playbook, contentAggregate, contentCatalog, playbookConfig)
    expect(serverRequests).to.have.lengthOf(1)
    expect(serverRequests[0]).to.equal(`/${TEST_JAR}`)
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    const paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)
  })

  it('should use remote bundle from cache on subsequent run', async () => {
    const url = `http://localhost:1337/${TEST_JAR}`
    let contentCatalog = await importJavadoc(...setupInputs(playbook, config(url)))
    expect(serverRequests).to.have.lengthOf(1)
    expect(serverRequests[0]).to.equal(`/${TEST_JAR}`)
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    let paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)

    contentCatalog = await importJavadoc(...setupInputs(playbook, config(url)))
    expect(serverRequests).to.have.lengthOf(1)
    paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)
  })

  it('should not download if fetch option is enabled and bundle is permanent', async () => {
    const url = `http://localhost:1337/${TEST_JAR}`
    const playbook = {
      runtime: { fetch: true },
    }
    let contentCatalog = await importJavadoc(...setupInputs(playbook, config(url)))
    expect(serverRequests).to.have.lengthOf(1)
    expect(serverRequests[0]).to.equal(`/${TEST_JAR}`)
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    const paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)

    contentCatalog = await importJavadoc(...setupInputs(playbook, config(url)))
    expect(serverRequests).to.have.lengthOf(1)
  })

  it('should download instead of using cache if fetch option is enabled and bundle is a snapshot', async () => {
    const playbook = {
      runtime: { fetch: true },
    }
    const url = `http://localhost:1337/${TEST_JAR}`
    let contentCatalog = await importJavadoc(...setupInputs(playbook, config(url, true)))
    expect(serverRequests).to.have.lengthOf(1)
    expect(serverRequests[0]).to.equal(`/${TEST_JAR}`)
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    let paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)

    contentCatalog = await importJavadoc(...setupInputs(playbook, config(url, true)))
    expect(serverRequests).to.have.lengthOf(2)
    expect(serverRequests[1]).to.equal(`/${TEST_JAR}`)
    paths = contentCatalog.getAll().map((file) => file.path)
    expect(paths).to.include.members(expectedFilePaths)
  })

  it('should throw error if remote jar cannot be found', async () => {
    const playbook = {
      runtime: { fetch: true },
    }
    const url = `http://localhost:1337/XX${TEST_JAR}`
    const importJavadocDeferred = await deferExceptions(importJavadoc, ...setupInputs(playbook, config(url)))
    const expectedMessage = `Unexpected statusCode 404 for ${url}`
    expect(importJavadocDeferred).to.throw(expectedMessage)
  })

  it('should cache bundle if a valid jar file', async () => {
    const playbook = {}
    const url = `http://localhost:1337/${TEST_JAR}`
    await importJavadoc(...setupInputs(playbook, config(url)))
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    const cachedBundleBasename = await fsp.readdir(JAVADOC_CACHE_DIR).then((entries) => entries[0])
    const cachedBundlePath = ospath.join(JAVADOC_CACHE_DIR, cachedBundleBasename, 'contents')
    const expectedContents = await fsp.readFile(ospath.join(FIXTURES_DIR, TEST_JAR))
    const actualContents = await fsp.readFile(cachedBundlePath)
    try {
      expect(actualContents).to.eql(expectedContents)
    } catch (e) {
      // NOTE showing the diff causes mocha to hang
      e.showDiff = false
      throw e
    }
  })

  it('should not cache bundle if not a valid jar file', async () => {
    expect(await deferExceptions(importJavadoc, ...setupInputs(playbook, config('http://localhost:1337/the-javadoc-jar.tar.gz')))).to.throw()
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.be.empty()
  })

  it('should throw error if bundle in cache is not a valid jar file', async () => {
    const url = `http://localhost:1337/${TEST_JAR}`
    await importJavadoc(...setupInputs(playbook, config(url)))
    expect(JAVADOC_CACHE_DIR)
      .to.be.a.directory()
      .and.not.be.empty()
    const cachedBundleBasename = await fsp.readdir(JAVADOC_CACHE_DIR).then((entries) => entries[0])
    const cachedBundlePath = ospath.join(JAVADOC_CACHE_DIR, cachedBundleBasename, 'contents')
    await fsp.copyFile(ospath.join(FIXTURES_DIR, 'the-javadoc-jar.tar.gz'), cachedBundlePath)

    expect(await deferExceptions(importJavadoc, ...setupInputs(playbook, config(url))))
      .to.throw(`Invalid ${BUNDLE_TYPE}: '${url}'`)
      .with.property('stack')
      .that.includes('not a valid zip file')
  })

  describe('custom cache dir', () => {
    const testCacheDir = async (cacheDir, dir) => {
      const customCacheDir = ospath.join(WORK_DIR, '.antora-cache', 'content', '2')
      const customUiCacheDir = ospath.join(customCacheDir, JAVADOC_CACHE_FOLDER)
      const playbook = {
        dir,
        runtime: { cacheDir },
      }
      const url = `http://localhost:1337/${TEST_JAR}`
      const contentCatalog = await importJavadoc(...setupInputs(playbook, config(url)))
      expect(JAVADOC_CACHE_DIR).to.not.be.a.path()
      expect(customUiCacheDir)
        .to.be.a.directory()
        .and.not.be.empty()
      const paths = contentCatalog.getAll().map((file) => file.path)
      expect(paths).to.include.members(expectedFilePaths)
    }

    it('should use custom cache dir relative to cwd (implicit)', async () => {
      await testCacheDir('.antora-cache')
    })

    it('should use custom cache dir relative to cwd (explicit)', async () => {
      await testCacheDir(ospath.join('~+', '.antora-cache'))
    })

    it('should use custom cache dir relative to directory of playbook file', async () => {
      process.chdir(os.tmpdir())
      await testCacheDir('./.antora-cache', WORK_DIR)
    })

    it('should use custom cache dir relative to user home', async () => {
      process.chdir(os.tmpdir())
      await testCacheDir('~' + ospath.sep + ospath.relative(os.homedir(), ospath.join(WORK_DIR, '.antora-cache')))
    })
  })
})
